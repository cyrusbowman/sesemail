'use strict';

var Promise = require('bluebird');
var AWS = require('aws-sdk');
var uuid = require('uuid');
var debug = require('debug')('sesEmail:S3Attachment');
var mime = require('mime');

const S3_URL = "https://s3.amazonaws.com";
const BUCKET = process.env.S3_BUCKET;
const FOLDER = process.env.S3_ATTACHMENTS_FOLDER;

const S3_CONFIG = {
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.S3_REGION || process.env.AWS_REGION
};

var s3 = Promise.promisifyAll(new AWS.S3(S3_CONFIG), {suffix: 'P'});

// Ensure bucket exists?
var bucket = s3.createBucketP({
	Bucket: BUCKET,
	ACL: 'private'
}).catch(function(err) {
	debug('Bucket ' + BUCKET + ' already created.');
});

/**
 * @constructor
 */
function S3Attachment(name, vals) {
	this.name = name;
	this.vals = vals;
}

/**
 * TODO: Better name?
 * Function to "attach" the attachment
 * Call its render, then puts it in S3
 *
 * @returns {{name: string, url: string}}
 */
S3Attachment.prototype.attach = function s3attach(vals) {
	var self = this;
	var attachmentUUID = uuid.v4();
	var key = FOLDER + '/' + attachmentUUID;

	var upload = this.render(self.vals || {})
		.then(function uploadFile(body) {

			var contentType = mime.lookup(self.name);
			var disposition = 'inline';
			if (contentType == 'application/octet-stream') {
				disposition = 'attachment';
			}

			return s3.putObjectP({
				Bucket: BUCKET,
				Key: key,
				ContentType: (contentType || 'application/octet-stream'),
				ContentDisposition: disposition+'; filename="'+self.name.replace(/"/g,'')+'"',
				ACL: 'public-read',
				Body: body
			});
		}).catch((err) => {
			debug('Failed to render attachment.', err);
			return {};
		});

	var obj = bucket.get('Location')
		.then(function(location){
			return {
				name: self.name,
				url: S3_URL + location + '/' + key,
				location: S3_URL + location + '/' + FOLDER,
				filename: attachmentUUID
			};
		});

	return upload.return(obj);
};

module.exports = S3Attachment;
