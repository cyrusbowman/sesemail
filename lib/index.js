'use strict';

var Email = require('./Email.js');
var S3Attachment = require('./S3Attachment');
module.exports = {
  Email: Email,
  S3Attachment: S3Attachment
};