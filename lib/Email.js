'use strict';

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var util = require('util');
var path = require('path');

var Promise = require('bluebird');
var _ = require('lodash');
var debug = require('debug')('sesEmail:Email');
var AWS = require('aws-sdk');
var html2text = require('html-to-text').fromString;
var Template = require('email-templates');

var MAX_ADDRESSES_PER_EMAIL = 50; //SES restriction

/*const SES_CONFIG = {
	accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION
};*/
// http://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/SES.html
var ses = Promise.promisifyAll(new AWS.SES(), { suffix: 'P' });

var DEF_RETURN_EMAIL = process.env.RETURN_ADDRESS;
var DEF_FROM = process.env.FROM_ADDRESS;

var DEF_HTML_TO_TEXT = {
	// Default to including tables in text version if they have class text
	tables: ['.text']
};
var EMAIL_REGEX = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
function validEmail(email) {
	if (email == null || email == '') return;
	//Check only email if title provided ie. 'TEST <example@test.com>'
	if (email.match(/.*<(.*)>.*/)) return email.match(/.*<(.*)>.*/)[1].match(EMAIL_REGEX);
	return email.match(EMAIL_REGEX);
}

//Resolve all promises and attach attachments
function resolveItAndHandleAttachments(obj) {
	if (obj instanceof Email.Attachment) {
		return obj.attach({});
	} else if (Array.isArray(obj)) {
		//Resolve the array
		return Promise.all(obj).then(function (obj) {
			//Resolve inside the array
			return Promise.map(obj, function (item) {
				return resolveItAndHandleAttachments(item);
			});
		});
	} else if (obj instanceof Promise) {
		return Promise.resolve(obj).then(function (val) {
			return resolveItAndHandleAttachments(val);
		});
	} else if ((typeof obj === 'undefined' ? 'undefined' : _typeof(obj)) === 'object' && obj != null) {
		//Resolve the object properties
		return Promise.props(obj).then(function (obj) {
			//Every key in obj is no longer a promise.
			//But a key could be an array of promises, or a object full of promises.
			//Check each property inside the object
			var promises = [];
			Object.keys(obj).map(function (key, index) {
				promises.push(resolveItAndHandleAttachments(obj[key]).then(function (val) {
					obj[key] = val;
				}));
			});
			return Promise.all(promises).then(function () {
				return obj;
			});
		});
	} else {
		return Promise.resolve(obj);
	}
}

// templ is a directory with a template named html.ext where ext is whatever
// is can also have a template text.ext for text emails
// and style.ext for adding css to html (less, scss etc.)
function Email(templ, subject, vals, options) {
	var opts = Promise.props(options || {});
	var template = Promise.join(templ, opts, function getTemplate(templ, opts) {
		if (typeof templ === 'string') {
			return new Template({ views: { root: templ }, juiceResources: { webResources: { relativeTo: path.resolve(templ) } } });
		} else if (templ instanceof Template) {
			return templ;
		} else {
			throw new TypeError('Invalid template');
		}
	});
	this.subject = subject;

	//Resolve any promises in vals and attach any attachments in vals
	this.vals = resolveItAndHandleAttachments(vals);

	var that = this;
	this.body = Promise.resolve(that.vals).then(function renderEmail(vals) {
		return template.call('render', 'html.hbs', vals);
	}).then(function h2topts(body) {
		return opts.get('htmlToTextOptions').then(function toText(h2topts) {
			h2topts = _.defaults({}, h2topts, DEF_HTML_TO_TEXT);
			return { text: html2text(body), html: body };
		});
	});
}

// addrs = {from: ?, to: ?, cc: ?, bcc: ?, replyTo: ?}
// From is string, others string or array of strings
Email.prototype.send = function send(addrs, callback) {
	var self = Promise.props(this);

	return Promise.join(self, addrs, function (self, addrs) {
		var from = addrs.from || DEF_FROM;
		var ret = addrs.ret || DEF_RETURN_EMAIL;
		addrs = _.mapValues(addrs, function (val) {
			return Array.isArray(val) ? val : [val];
		});
		var toAddressesGroups = _.chunk(_.chain(addrs.to || []).omitBy(_.isEmpty).values().filter(validEmail).value(), MAX_ADDRESSES_PER_EMAIL);
		var ccAddressesGroups = _.chunk(_.chain(addrs.cc || []).omitBy(_.isEmpty).values().filter(validEmail).value(), MAX_ADDRESSES_PER_EMAIL);
		var bccAddressesGroups = _.chunk(_.chain(addrs.bcc || []).omitBy(_.isEmpty).values().filter(validEmail).value(), MAX_ADDRESSES_PER_EMAIL);
		var maxGroups = toAddressesGroups.length;
		if (ccAddressesGroups.length > maxGroups) maxGroups = ccAddressesGroups.length;
		if (bccAddressesGroups.length > maxGroups) maxGroups = bccAddressesGroups.length;

		var emailsPromises = [];
		console.log('Emailing', maxGroups, 'groups');
		for (var i = 0; i < maxGroups; i++) {
			console.log('Sending to group:', i, toAddressesGroups[i]);
			var ToAddresses = toAddressesGroups[i] || [];
			var CcAddresses = ccAddressesGroups[i] || [];
			var BccAddresses = bccAddressesGroups[i] || [];
			emailsPromises.push(ses.sendEmailP({
				Source: from,
				Destination: {
					ToAddresses: ToAddresses,
					CcAddresses: CcAddresses,
					BccAddresses: BccAddresses
				},
				ReplyToAddresses: addrs.replyto || [],
				ReturnPath: ret,
				Message: {
					Subject: {
						Data: self.subject
					},
					Body: {
						Html: { Data: self.body.html },
						Text: { Data: self.body.text }
					}
				}
			}));
		}
		return Promise.all(emailsPromises).then(function (responses) {
			var response = responses[0]; //TODO maybe return all responces, check if using ses though
			var ret = {
				ses: response,
				vals: self.vals
			};
			console.log('Email sent: ' + self.subject);
			return ret;
		}).catch(function (err) {
			console.log('ERROR: Email failed to send: ' + self.subject, err);
			return {};
		});
	}).nodeify(callback);
};

Email.extend = function extend(templ, subject, vals, opts) {
	var template = Promise.join(templ, opts, function (templ, opts) {
		return new Template({ views: { root: templ }, juiceResources: { webResources: { relativeTo: path.resolve(templ) } } });
	});
	var EE = function ExtendedEmail(vs) {
		var vvals = Promise.join(vs, vals, function (vs, vals) {
			return _.assign(vs, vals);
		});
		Email.call(this, template, subject, vvals);
	};
	util.inherits(EE, Email);
	return EE;
};

// Sends verification email(s) to address(es) if not already verified
// From addresses must be verified for SES
// 'To' addresses must be verified while in sandbox (http://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses.html)
Email.verify = function verify(addrs, callback) {
	return Promise.resolve(addrs).then(function (emails) {
		if (typeof emails === 'string') {
			return emails;
		}
		return _.values(emails);
	}).then(function (emails) {
		return ses.getIdentityVerificationAttributesP({
			Identities: emails
		}).get('VerificationAttributes').then(function (res) {
			return emails.map(function (email) {
				var status = res[email] && res[email].VerificationStatus;
				return { email: email, status: status };
			});
		});
	}).filter(function (res) {
		return res.status !== 'Success';
	}).map(_.property('email')).each(function (email) {
		return console.log('Email "', email, '" has not been verified');
	}).each(function (email) {
		return ses.verifyEmailIdentityP({ EmailAddress: email }).tap(function () {
			return console.log('Verification email sent to', email);
		});
	}).nodeify(callback);
};

Email.Attachment = require('./S3Attachment');

module.exports = Email;