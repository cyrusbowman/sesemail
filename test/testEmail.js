'use strict';

var path = require('path');
var debug = require('debug')('snsEmail:test:testEmail');
var _ = require('lodash');
var sampleData = require('./testTemplate/sampleData.js');
var Email = require('../').Email;

var TEMPLATE = path.join(__dirname, './testTemplate');
var SUBJECT = 'Contact Added';
var VALS = {
	title: 'Contact Added'
};

var Email01 = Email.extend(TEMPLATE, SUBJECT, VALS);

function contactAddedEvent() {
		var email = new Email01({
			customer: sampleData.customer,
			createUser: _.get(sampleData, 'createUser'),
			person: _.omitBy(sampleData.person, _.isNull),
			parentSummary: sampleData.parentSummary ? "Yes" : "No",
			qaCreation: sampleData.qaCreation ? "Yes" : "No",
			qaUpdates: sampleData.qaUpdate ? "Yes" : "No",
			weeklySummary: sampleData.weeklySummary ? "Yes" : "No",
		})
		var sendTo = ['cyrusbow@gmail.com'];
		return email.send({
			to: sendTo
		});
};
contactAddedEvent();
