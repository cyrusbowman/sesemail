module.exports = {
	customer: "Yoolim Industrial Co.",
	createUser: {
		firstName: "Cyrus",
		lastName: "Bowman"
	},
	person: {
		firstName: "Kim",
		lastName: "Taewon",
		email: "biz@yoolim.kr",
		phone: "538-343-2002",
		cell: "233-998-2889",
		fax: "123-456-4433",
	},
	parentSummary: "No",
	qaCreation: "Yes",
	qaUpdates: "Yes",
	weeklySummary: "Yes"
}
